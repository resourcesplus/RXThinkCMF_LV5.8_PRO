<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Http\Controllers;


use App\Models\ConfigModel;
use App\Models\ConfigDataModel;
use App\Services\ConfigWebService;
use Illuminate\Http\Request;

/**
 * 网站配置-控制器
 * @author 牧羊人
 * @since 2020/9/1
 * Class ConfigWebController
 * @package App\Http\Controllers
 */
class ConfigWebController extends Backend
{
    /**
     * 构造函数
     * @param Request $request
     * @since 2020/9/1
     * ConfigWebController constructor.
     * @author 牧羊人
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new ConfigDataModel();
        $this->service = new ConfigWebService();
    }

    /**
     * 系统配置管理
     * @return
     */
    public function index()
    {
        if (IS_POST) {
            $result = $this->service->config();
            return $result;
        }
        // 配置ID
        $configId = request()->input('config_id', 1);

        // 获取配置列表
        $configModel = new ConfigModel();
        $configList = $configModel->where(['mark' => 1])->get()->toArray();
        view()->share("configList", $configList);

        // 获取元素列表
        $list = $this->model->getList([
            ['config_id', '=', $configId],
            ['status', '=', 1],
        ], [['id', 'asc']]);
        if ($list) {
            foreach ($list as &$val) {
                if ($val['type'] == 'checkbox') {
                    // 复选框
                    $val['format_name'] = "{$val['name']}__checkbox|name|id";

                    // 组件数据源
                    $optionsList = [];
                    if ($val['options']) {
                        $options = preg_split("/[\r\n]/", $val['options']);
                        if ($options && is_array($options)) {
                            foreach ($options as $v) {
                                $item = explode(':', $v);
                                $optionsList[] = [
                                    'id' => $item[0],
                                    'name' => $item[1],
                                ];
                            }
                        }
                    }
                    $val['format_options'] = $optionsList;

                    // 选中的值
                    if ($val['value']) {
                        $val['format_value'] = explode(',', $val['value']);
                    }
                } else if ($val['type'] == 'radio') {
                    // 单选
                    $val['format_name'] = "{$val['name']}|name|id";

                    // 组件数据源
                    $optionsList = [];
                    if ($val['options']) {
                        $options = preg_split("/[\r\n]/", $val['options']);
                        if ($options && is_array($options)) {
                            foreach ($options as $v) {
                                $item = explode(':', $v);
                                $optionsList[] = [
                                    'id' => $item[0],
                                    'name' => $item[1],
                                ];
                            }
                        }
                    }
                    $val['format_options'] = $optionsList;
                } else if ($val['type'] == 'select') {
                    // 下拉选择
                    $val['format_name'] = "{$val['name']}|1|{$val['title']}|name|id";

                    // 组件数据源
                    $optionsList = [];
                    if ($val['options']) {
                        $options = preg_split("/[\r\n]/", $val['options']);
                        if ($options && is_array($options)) {
                            foreach ($options as $v) {
                                $item = explode(':', $v);
                                $optionsList[] = [
                                    'id' => $item[0],
                                    'name' => $item[1],
                                ];
                            }
                        }
                    }
                    $val['format_options'] = $optionsList;
                } else if ($val['type'] == 'image') {
                    // 单图处理
                    $val['value'] = get_image_url($val['value']);
                } else if ($val['type'] == 'images') {
                    // 图集
                    $imgsList = $val['value'] ? unserialize($val['value']) : [];
                    if (!empty($imgsList)) {
                        foreach ($imgsList as &$vo) {
                            $vo = get_image_url($vo);
                        }
                    }
                    $val['value'] = $imgsList;
                }
            }
        }
        // 渲染数据
        view()->share("list", $list);
        return view("configweb.index", ['config_id' => $configId]);
    }
}
