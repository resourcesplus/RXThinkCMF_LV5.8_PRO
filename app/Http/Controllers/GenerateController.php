<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Http\Controllers;

use App\Services\GenerateService;
use Illuminate\Http\Request;

/**
 * 代码生成器-控制器
 * @author 牧羊人
 * @since 2020/9/1
 * Class GenerateController
 * @package App\Http\Controllers
 */
class GenerateController extends Backend
{
    /**
     * 构造函数
     * @param Request $request
     * @since 2020/9/1
     * GenerateController constructor.
     * @author 牧羊人
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->service = new GenerateService();
    }

    /**
     * 一键生成模块
     * @return mixed
     * @since 2020/9/1
     * @author 牧羊人
     */
    public function generate()
    {

        if (IS_POST) {
            // 参数
            $param = request()->all();
            return $this->service->generate($param);
        }
    }

    /**
     * 批量生成
     * @return array|void
     */
    public function batchGenerate()
    {
        if (IS_POST) {
            // 参数
            $param = request()->all();
            // 表信息
            $tables = isset($param['tables']) ? $param['tables'] : [];
            if (empty($tables)) {
                return message("请选择数据表", false);
            }
            $tableList = explode(",", $tables);
            foreach ($tableList as $val) {
                $item = explode("|", $val);
                $data = [
                    'name' => $item[0],
                    'comment' => $item[1],
                ];
                $this->service->generate($data);
            }
            return message(sprintf("本次共生成【%d】个模块", count($tableList)));
        }
    }
}
