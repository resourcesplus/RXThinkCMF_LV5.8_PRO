<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Http\Controllers;

use App\Models\LayoutDescModel;
use App\Services\LayoutDescService;
use Illuminate\Http\Request;

/**
 * 布局描述-控制器
 * @author 牧羊人
 * @since 2020/8/30
 * Class LayoutDescController
 * @package App\Http\Controllers
 */
class LayoutDescController extends Backend
{
    /**
     * 构造函数
     * @param Request $request
     * @since 2020/8/30
     * LayoutDescController constructor.
     * @author 牧羊人
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new LayoutDescModel();
        $this->service = new LayoutDescService();
    }

    /**
     * 获取布局描述
     * @return array
     * @since 2020/8/30
     * @author 牧羊人
     */
    public function getLayoutDescList()
    {
        // 站点ID
        $itemId = request()->input("item_id", 0);
        $list = $this->model->where(['item_id' => $itemId, 'mark' => 1])->orderBy("sort")->get()->toArray();
        return message("操作成功", true, $list);
    }
}
