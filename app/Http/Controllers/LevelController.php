<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Http\Controllers;


use App\Exports\Export;
use App\Imports\LevelImport;
use App\Models\LevelModel;
use App\Services\LevelService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * 职级管理-控制器
 * @author 牧羊人
 * @since 2020/8/28
 * Class LevelController
 * @package App\Http\Controllers
 */
class LevelController extends Backend
{
    /**
     * 构造函数
     * @param Request $request
     * @since 2020/8/28
     * LevelController constructor.
     * @author 牧羊人
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new LevelModel();
        $this->service = new LevelService();
    }

    /**
     * 导入Excel
     * @author 牧羊人
     * @since 2021/5/25
     */
    public function btnImport(Request $request)
    {
        $result = upload_file($request);
        if (!$result['success']) {
            return message($result['msg'], false);
        }
        // 文件路径
        $file_path = $result['data']['file_path'];
        if (!$file_path) {
            return message("文件上传失败", false);
        }
        // 导入Excel
        Excel::import(new LevelImport(), $file_path);
        return message("导入成功", true);
    }

    /**
     * 导出Excel
     * @author 牧羊人
     * @since 2022/1/30
     */
    public function btnExport()
    {
        // 参数
        $param = request()->all();
        // 文件名称
        $fileName = date('YmdHis') . "_职级管理" . '.xlsx';
        // 表格头
        $header = ['职级ID', '职级名称', '职级状态', '排序'];
        // 获取数据源
        $result = $this->model->where("mark", "=", 1)->get()->toArray();
        // 重组数据源
        $list = [];
        if (!empty($result)) {
            foreach ($result as $key => $val) {
                $data = [];
                $data['id'] = $val['id'];
                $data['name'] = $val['name'];
                $data['status'] = $val['status'] == 1 ? "在用" : "停用";
                $data['sort'] = $val['sort'];
                $list[] = $data;
            }
        }
        //执行导出
        return Excel::download(new Export($list, $header, "职级列表"), $fileName);
    }

}
