<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Http\Controllers;

use App\Models\ActionLogModel;
use App\Services\LoginService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * 系统登录-控制器
 * @author 牧羊人
 * @since 2020/8/29
 * Class LoginController
 * @package App\Http\Controllers
 */
class LoginController extends Backend
{
    /**
     * 构造方法
     *
     * @param Request $request
     * @author 牧羊人
     * @date 2019-05-24
     */
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->service = new LoginService();
    }

    /**
     * 登录API
     *
     * @author 牧羊人
     * @date 2019-05-24
     */
    function login(Request $request)
    {
        if (IS_POST) {
            $result = $this->service->login($request);
            return $result;
        }
        return view('login');
    }

    /**
     * 退出系统
     * @return
     * @since 2020/8/30
     * @author 牧羊人
     */
    public function logout()
    {
        // 清空SESSION
        session()->put("adminId", null);
        // 记录退出日志
        ActionLogModel::setTitle("系统退出");
        ActionLogModel::record();
        // 跳转登录页
        return redirect(url('/login/login'));
    }


}
