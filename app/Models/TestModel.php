<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Models;

/**
 * 测试-模型
 * @author 牧羊人
 * @date 2019/5/28
 * Class TestModel
 * @package App\Models
 */
class TestModel extends BaseModel
{
    // 设置数据表
    protected $table = 'admin';

    // 禁用缓存
//    protected $is_cache = false;

    /**
     * 获取缓存信息
     * @param int $id 记录ID
     * @return array 返回结果
     * @author 牧羊人
     * @date 2019/5/28
     */
    public function getInfo($id)
    {
        $info = parent::getInfo($id);
        if ($info) {
            // TODO...
        }
        return $info;
    }
}
