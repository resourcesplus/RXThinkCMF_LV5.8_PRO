<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Models;

/**
 * 会员管理-模型
 * @author 牧羊人
 * @since 2020/8/28
 * Class UserModel
 * @package App\Models
 */
class UserModel extends BaseModel
{
    // 设置数据表
    protected $table = "user";

    /**
     * 获取记录信息
     * @param int $id 记录ID
     * @return array|string
     * @author 牧羊人
     * @since 2020/8/30
     */
    public function getInfo($id)
    {
        $info = parent::getInfo($id);
        if ($info) {
            // 会员头像
            if ($info['avatar']) {
                $info['avatar_url'] = get_image_url($info['avatar']);
            }

            // 会员性别
            if ($info['gender']) {
                $info['gender_name'] = config('admin.gender_list')[$info['gender']];
            }

            // 设备类型
            $info['device_name'] = config("admin.user_device")[$info['device']];

            // 用户来源
            if ($info['source']) {
                $info['source_name'] = config("admin.user_source")[$info['source']];
            }

            // 格式化出生日期
            if ($info['birthday']) {
                $info['format_birthday'] = datetime($info['birthday'], "Y-m-d");
            }

            // 获取城市名称
            if ($info['district_id']) {
                $cityModel = new CityModel();
                $cityName = $cityModel->getCityName($info['district_id'], " ");
                if ($cityName) {
                    $info['city_area'] = $cityName;
                    $cityItem = explode(" ", $cityName);
                    $info['province_name'] = $cityItem[0];
                    $info['city_name'] = $cityItem[1];
                    $info['district_name'] = $cityItem[2];
                }
            }
        }
        return $info;
    }
}
