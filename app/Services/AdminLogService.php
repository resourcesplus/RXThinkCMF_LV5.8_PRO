<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Services;


use App\Models\ActionLogModel;

/**
 * 登录日志-服务类
 * @author 牧羊人
 * @since 2020/8/30
 * Class AdminLogService
 * @package App\Services
 */
class AdminLogService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/8/30
     * AdminLogService constructor.
     */
    public function __construct()
    {
        $this->model = new ActionLogModel();
    }

    /**
     * 获取数据列表
     * @return array
     * @since 2020/8/30
     * @author 牧羊人
     */
    public function getList()
    {
        // 查询条件
        $param = request()->all();

        // 查询条件
        $query = $this->model->where(function ($query) {
            $query->where('title', 'like', '%系统登录%')
                ->orWhere('title', 'like', '%系统退出%');
        });
        // 日志标题模糊查询
        $title = isset($param['title']) ? $param['title'] : '';
        if ($title) {
            $query = $query->where("title", "like", "%{$title}%");
        }

        //获取数据总数
        $count = $query->count();

        // 获取数据列表
        $offset = (PAGE - 1) * PERPAGE;
        $result = $query->orderByDesc("id")->offset($offset)->limit(PERPAGE)->get()->toArray();
        //返回结果
        $message = array(
            "msg" => '操作成功',
            "code" => 0,
            "data" => $result,
            "count" => $count,
        );
        return $message;
    }
}
