<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Services;

use App\Models\AdminModel;
use App\Models\AdminRomModel;
use App\Models\MenuModel;

/**
 * 权限管理-服务类
 * @author 牧羊人
 * @since 2020/8/28
 * Class AdminRomService
 * @package App\Services
 */
class AdminRomService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/8/28
     * AdminRomService constructor.
     */
    public function __construct()
    {
        $this->model = new AdminRomModel();
    }

    /**
     * 获取菜单权限列表
     * @return array
     * @since 2020/8/28
     * @author 牧羊人
     */
    public function getList()
    {
        // 类型
        $type = request()->input("type");
        // 类型ID
        $typeId = request()->input('typeId');

        // 获取所有菜单
        $menuModel = new MenuModel();
        $menuList = $menuModel->getList([], [['sort', 'asc']]);
        // 获取有权限的菜单
        $where = [
            ['type', '=', $type],
            ['type_id', '=', $typeId],
        ];
        $adminRomModel = new AdminRomModel();
        $permissionList = $adminRomModel->getList($where, [['menu_id', 'asc']]);
        $checkList = [];
        if ($permissionList) {
            $checkList = array_column($permissionList, "menu_id");
        }
        $list = [];
        if (!empty($menuList)) {
            foreach ($menuList as $val) {
                $data = [];
                $data['id'] = $val['id'];
                $data['name'] = trim($val['name']);
                $data['pId'] = $val['pid'];
                if (in_array($val['id'], $checkList)) {
                    $data['checked'] = true;
                } else {
                    $data['checked'] = false;
                }
                $data['open'] = true;
                $list[] = $data;
            }
        }
        return message("操作成功", true, $list);
    }

    /**
     * 设置权限
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\BindParamException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @since 2020/7/3
     * @author 牧羊人
     */
    public function setPermission()
    {
        // 参数
        $param = request()->all();
        // 类型
        $type = intval($param['type']);
        // 类型ID
        $typeId = intval($param['typeId']);
        if (!$typeId) {
            return message("类型ID不能为空", false);
        }
        // 删除现有的权限
        $where = [
            ['type', '=', $type],
            ['type_id', '=', $typeId],
        ];
        $adminRomModel = new AdminRomModel();
        $permissionList = $adminRomModel->getList($where, [['menu_id', 'asc']]);
        if ($permissionList) {
            $itemList = array_column($permissionList, "id");
            $adminRomModel->deleteAll($itemList, true);
        }
        // 权限ID
        $authIds = trim($param['authIds']);
        if ($authIds) {
            $itemArr = explode(',', $authIds);
            foreach ($itemArr as $val) {
                $data = [
                    'type' => $type,
                    'type_id' => $typeId,
                    'menu_id' => $val,
                ];
                $adminRomModel = new AdminRomModel();
                $adminRomModel->edit($data);
            }
        }
        return message("操作成功");
    }

    /**
     * 获取权限列表
     * @param $adminId 人员ID
     * @return array|mixed
     * @author 牧羊人
     * @since 2021/2/1
     */
    public function getPermissionList($adminId)
    {
        if ($adminId == 1) {
            // 管理员
            $menuModel = new MenuModel();
            $menuList = $menuModel->getChilds(0);
            return $menuList;
        } else {
            // 非管理员
            $adminModel = new AdminModel();
            $adminInfo = $adminModel->getInfo($adminId);
            $menuList = $this->model->getPermissionMenu($adminInfo['role_ids'], $adminId, 1, 0);
            return $menuList;

        }
    }

    /**
     * 获取权限节点
     * @param $adminId 用户ID
     * @return array|mixed
     * @author 牧羊人
     * @since 2021/2/1
     */
    public function getPermissionFuncList($adminId)
    {
        if ($adminId == 1) {
            // 管理员
            $menuModel = new MenuModel();
            $menuList = $menuModel->where("type", 4)
                ->where("mark", 1)
                ->get('permission')
                ->toArray();
            $permissionList = array_key_value($menuList, "permission");
            return $permissionList;
        } else {
            // 非管理员
            $adminModel = new AdminModel();
            $adminInfo = $adminModel->getInfo($adminId);
            $permissionList = $this->model->getPermissionFuncList($adminInfo['role_ids'], $adminId);
            return $permissionList;
        }
    }

}
