<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Services;


use App\Models\ActionLogModel;
use App\Models\AdminModel;
use Illuminate\Support\Facades\Validator;

class LoginService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/8/26
     * LoginService constructor.
     */
    public function __construct()
    {
        $this->model = new AdminModel();
    }

    /**
     * 系统登录
     * @return array
     * @since 2020/8/26
     * @author 牧羊人
     */
    public function login($request)
    {
        // 参数
        $param = request()->all();
        // 用户名
        $username = trim($param['username']);
        // 密码
        $password = trim($param['password']);
        // 验证规则
        $rules = [
            'username' => 'required|min:2|max:20',
            'password' => 'required|min:6|max:20',
            'captcha' => ['required', 'captcha'],
        ];
        // 规则描述
        $messages = [
            'required' => ':attribute为必填项',
            'min' => ':attribute长度不符合要求',
            'captcha.required' => '验证码不能为空',
            'captcha.captcha' => '请输入正确的验证码',
        ];
        // 验证
        $validator = Validator::make($param, $rules, $messages, [
            'username' => '用户名称',
            'password' => '登录密码'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            foreach ($errors as $key => $value) {
                return message($value[0], false);
            }
        }

        // 用户验证
        $info = $this->model->getOne([
            ['username', '=', $username],
        ]);
        if (!$info) {
            return message('您的登录用户名不存在', false, 'username');
        }
        // 密码校验
        $password = get_password($password . $username);
        if ($password != $info['password']) {
            return message("您的登录密码不正确", false, "password");
        }
        // 使用状态校验
        if ($info['status'] != 1) {
            return message("您的帐号已被禁用", false);
        }

        // 设置日志标题
        ActionLogModel::setTitle("系统登录");
        ActionLogModel::record();

        // 本地SESSION存储登录信息
        session()->put("adminId", $info['id']);
        return message('登录成功', true);
    }
}
