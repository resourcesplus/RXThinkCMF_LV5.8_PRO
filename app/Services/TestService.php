<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Services;

use App\Models\AdminModel;
use App\Http\Requests\AdminRequest;

/**
 * 测试-服务类
 * @author 牧羊人
 * @date 2019/5/24
 * Class TestService
 * @package App\Services
 */
class TestService extends BaseService
{
    /**
     * 构造方法
     * @author 牧羊人
     * @date 2019/5/24
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new AdminModel();
        $this->validate = new AdminRequest();
    }

    /**
     * 获取数据列表
     * @param $param 参数
     * @param $admin_id 人员ID
     * @return array 返回结果
     * @author 牧羊人
     * @date 2019/5/24
     */
    public function getAdminList($param, $admin_id)
    {
        $map = [
            'query' => [
                ['realname', 'like', '%相约%'],
            ],
            'sort' => [
                ['id', 'desc']
            ],
            'limit' => '0,2',
        ];
        $result = $this->model->getData($map, function ($info) {
            return [
                'id' => $info['id'],
                'realname' => $info['realname'],
            ];
        });
        return message(MESSAGE_OK, true, $result);
    }

    /**
     * 获取分页数据
     * @param $param 参数
     * @param $admin_id 人员ID
     * @return array 返回结果
     * @author 牧羊人
     * @date 2019/5/27
     */
    public function getAdminList2($param, $admin_id)
    {
        $map = [
            'query' => [
                ['realname', 'like', '%相约%'],
            ],
            'sort' => [
                ['id', 'desc']
            ],
            'page' => 1,
            'perpage' => 1,
        ];
        $result = $this->model->pageData($map, function ($info) {
            return [
                'id' => $info['id'],
                'realname' => $info['realname'],
            ];
        });
        return $result;
    }
}
