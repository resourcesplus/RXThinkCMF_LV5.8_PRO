@extends('public.form')
@section('content')
<form class="layui-form model-form" action="">
    <input name="id" id="id" type="hidden" value="{{isset($info['id']) ? $info['id'] : 0}}">
    <input name="pid" id="pid" type="hidden" value="{{isset($info['pid']) ? $info['pid'] : 0}}">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">部门名称：</label>
            <div class="layui-input-inline">
                <input name="name" value="{{isset($info['name']) ? $info['name'] : ''}}" lay-verify="required" autocomplete="off" placeholder="请输入部门名称" class="layui-input" type="text">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">部门全称：</label>
            <div class="layui-input-inline">
                <input name="fullname" value="{{isset($info['fullname']) ? $info['fullname'] : ''}}" lay-verify="required" autocomplete="off" placeholder="请输入部门全称" class="layui-input" type="text">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">部门编码：</label>
            <div class="layui-input-inline">
                <input name="code" value="{{isset($info['code']) ? $info['code'] : ''}}" lay-verify="required" autocomplete="off" placeholder="请输入部门编码" class="layui-input" type="text">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">上级部门：</label>
            <div class="layui-input-inline">
                @render('SelectComponent', ['name'=>'pid|0|上级部门|name|id','data'=>$deptList,'value'=>isset($info['pid']) ? $info['pid'] : 0])
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">部门类型：</label>
            <div class="layui-input-inline">
                @render('SelectComponent', ['name'=>'type|1|部门类型|name|id','data'=>"1=公司,2=子公司,3=部门,4=小组",'value'=>isset($info['type']) ? $info['type'] : 1])
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">排序号：</label>
            <div class="layui-input-inline">
                <input name="sort" value="{{isset($info['sort']) ? $info['sort'] : 125}}" lay-verify="required|number" autocomplete="off" placeholder="请输入序号" class="layui-input" type="text">
            </div>
        </div>
    </div>
    <div class="layui-form-item layui-form-text" style="width:625px;">
        <label class="layui-form-label">备注：</label>
        <div class="layui-input-block">
            <textarea name="note" placeholder="请输入备注" class="layui-textarea">{{isset($info['note']) ? $info['note'] : ''}}</textarea>
        </div>
    </div>
    @render('SubmitComponent', ['name'=>'submit|立即保存,close|关闭'])
</form>
@endsection
