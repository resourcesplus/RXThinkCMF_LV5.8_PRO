﻿@extends('public.form')
@section('content')
<form class="layui-form model-form" action="">
    <input name="id" id="id" type="hidden" value="{{isset($info['id']) ? $info['id'] : 0}}">
        
    <div class="layui-form-item">
        <label class="layui-form-label">头像：</label>
        @render('UploadImageComponent', ['name'=>'avatar|头像|90x90|建议上传尺寸450x450|450x450','value'=>isset($info['avatar']) ? $info['avatar'] : ''])
    </div>
            
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">测试名称：</label>
            <div class="layui-input-inline">
            
                <input name="name" value="{{isset($info['name']) ? $info['name'] : ''}}" lay-verify="required" autocomplete="off" placeholder="请输入测试名称" class="layui-input" type="text">
            
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">性别：</label>
            <div class="layui-input-inline">
                            
                @render('SelectComponent', ['name'=>'gender|1|性别|name|id','data'=>'1=男,2=女,3=保密','value'=>isset($info['gender']) ? $info['gender'] : 1])
                        
            </div>
        </div>
        
    </div>
            
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">类型：</label>
            <div class="layui-input-inline">
                            
                @render('SelectComponent', ['name'=>'type|1|类型|name|id','data'=>'1=董事长,2=总经理,3=部门总监,4=部门经理,5=部门主管,6=普工','value'=>isset($info['type']) ? $info['type'] : 1])
                            
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">状态：</label>
            <div class="layui-input-inline">
                            
                @render('SwitchComponent', ['name'=>'status','title'=>'正常|停用','value'=>isset($info['status']) ? $info['status'] : 1])
                        
            </div>
        </div>
        
    </div>
            
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">是否VIP：</label>
            <div class="layui-input-inline">
                            
                @render('SwitchComponent', ['name'=>'is_vip','title'=>'是|否','value'=>isset($info['is_vip']) ? $info['is_vip'] : 2])
                            
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">显示顺序：</label>
            <div class="layui-input-inline">
            
                <input name="sort" value="{{isset($info['sort']) ? $info['sort'] : 0}}" lay-verify="required|number" autocomplete="off" placeholder="请输入显示顺序" class="layui-input" type="text">
            
            </div>
        </div>
        
    </div>
            
    <div class="layui-form-item layui-form-text" style="width:625px;">
        <label class="layui-form-label">备注：</label>
        <div class="layui-input-block">
            <textarea name="note" placeholder="请输入备注" class="layui-textarea">{{isset($info['note']) ? $info['note'] : ''}}</textarea>
            
        </div>
    </div>
        
    @render('SubmitComponent', ['name'=>'submit|立即保存,close|关闭'])
</form>
@endsection
