<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_LV5.8_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ActionLogController;
use App\Http\Controllers\AdController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminLogController;
use App\Http\Controllers\AdminRomController;
use App\Http\Controllers\AdSortController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\ConfigDataController;
use App\Http\Controllers\ConfigWebController;
use App\Http\Controllers\DeptController;
use App\Http\Controllers\DictController;
use App\Http\Controllers\DictDataController;
use App\Http\Controllers\GenerateController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ItemCateController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\LayoutController;
use App\Http\Controllers\LayoutDescController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\LinkController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\NoticeController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StatisticsController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserLevelController;

Route::get('/', function () {
    return view('welcome');
});

// 登录
Route::get('login/login', "LoginController@login");
Route::post('login/login', "LoginController@login");
Route::get('login/logout', "LoginController@logout");

// 用户登录中间件
Route::group(['middleware' => ['admin.login']], function () {

    // 文件上传
    Route::prefix('upload')->group(function () {
        Route::post('uploadImage', [UploadController::class, 'uploadImage']);
        Route::post('uploadFile', [UploadController::class, 'uploadFile']);
    });

    // 系统主页
    Route::get('/', [IndexController::class, 'index']);
    Route::prefix('index')->group(function () {
        Route::get('index', [IndexController::class, 'index']);
        Route::get('main', [IndexController::class, 'main']);
        Route::any('userinfo', [IndexController::class, 'userInfo']);
        Route::post('updatePwd', [IndexController::class, 'updatePwd']);
    });

    // 欢迎页
    Route::prefix('main')->group(function () {
        Route::get('index', [MainController::class, 'index']);
    });

    // 职级管理
    Route::prefix('level')->group(function () {
        Route::any('index', [LevelController::class, 'index']);
        Route::any('edit', [LevelController::class, 'edit']);
        Route::post('drop', [LevelController::class, 'drop']);
        Route::post('batchDrop', [LevelController::class, 'batchDrop']);
        Route::post('setStatus', [LevelController::class, 'setStatus']);
        Route::post('btnImport', [LevelController::class, 'btnImport']);
        Route::get('btnExport', [LevelController::class, 'btnExport']);
    });

    // 岗位管理
    Route::prefix('position')->group(function () {
        Route::any('index', [PositionController::class, 'index']);
        Route::any('edit', [PositionController::class, 'edit']);
        Route::post('drop', [PositionController::class, 'drop']);
        Route::post('batchDrop', [PositionController::class, 'batchDrop']);
        Route::post('setStatus', [PositionController::class, 'setStatus']);
    });

    // 角色管理
    Route::prefix('role')->group(function () {
        Route::any('index', [RoleController::class, 'index']);
        Route::any('edit', [RoleController::class, 'edit']);
        Route::post('drop', [RoleController::class, 'drop']);
        Route::post('batchDrop', [RoleController::class, 'batchDrop']);
    });

    // 菜单管理
    Route::prefix('menu')->group(function () {
        Route::any('index', [MenuController::class, 'index']);
        Route::any('edit', [MenuController::class, 'edit']);
        Route::post('drop', [MenuController::class, 'drop']);
    });

    // 菜单权限
    Route::prefix('adminrom')->group(function () {
        Route::get('index', [AdminRomController::class, 'index']);
        Route::post('setPermission', [AdminRomController::class, 'setPermission']);
    });

    // 人员管理
    Route::prefix('admin')->group(function () {
        Route::any('index', [AdminController::class, 'index']);
        Route::any('edit', [AdminController::class, 'edit']);
        Route::post('drop', [AdminController::class, 'drop']);
        Route::post('batchDrop', [AdminController::class, 'batchDrop']);
        Route::post('resetPwd', [AdminController::class, 'resetPwd']);
    });

    // 登录日志
    Route::prefix('adminlog')->group(function () {
        Route::any('index', [AdminLogController::class, 'index']);
        Route::post('drop', [AdminLogController::class, 'drop']);
        Route::post('batchDrop', [AdminLogController::class, 'batchDrop']);
    });

    // 操作日志
    Route::prefix('actionlog')->group(function () {
        Route::any('index', [ActionLogController::class, 'index']);
        Route::post('drop', [ActionLogController::class, 'drop']);
        Route::post('batchDrop', [ActionLogController::class, 'batchDrop']);
    });

    // 部门管理
    Route::prefix('dept')->group(function () {
        Route::any('index', [DeptController::class, 'index']);
        Route::any('edit', [DeptController::class, 'edit']);
        Route::post('drop', [DeptController::class, 'drop']);
        Route::post('batchDrop', [DeptController::class, 'batchDrop']);
    });

    // 会员等级
    Route::prefix('userlevel')->group(function () {
        Route::any('index', [UserLevelController::class, 'index']);
        Route::any('edit', [UserLevelController::class, 'edit']);
        Route::post('drop', [UserLevelController::class, 'drop']);
        Route::post('batchDrop', [UserLevelController::class, 'batchDrop']);
    });

    // 会员管理
    Route::prefix('user')->group(function () {
        Route::any('index', [UserController::class, 'index']);
        Route::any('edit', [UserController::class, 'edit']);
        Route::post('drop', [UserController::class, 'drop']);
        Route::post('batchDrop', [UserController::class, 'batchDrop']);
        Route::post('setStatus', [UserController::class, 'setStatus']);
    });

    // 友链管理
    Route::prefix('link')->group(function () {
        Route::any('index', [LinkController::class, 'index']);
        Route::any('edit', [LinkController::class, 'edit']);
        Route::post('drop', [LinkController::class, 'drop']);
        Route::post('batchDrop', [LinkController::class, 'batchDrop']);
    });

    // 字典管理
    Route::prefix('dict')->group(function () {
        Route::any('index', [DictController::class, 'index']);
        Route::any('edit', [DictController::class, 'edit']);
        Route::post('drop', [DictController::class, 'drop']);
        Route::post('batchDrop', [DictController::class, 'batchDrop']);
    });

    // 字典数据
    Route::prefix('dictdata')->group(function () {
        Route::any('index', [DictDataController::class, 'index']);
        Route::any('edit', [DictDataController::class, 'edit']);
        Route::post('drop', [DictDataController::class, 'drop']);
        Route::post('batchDrop', [DictDataController::class, 'batchDrop']);
    });

    // 配置管理
    Route::prefix('config')->group(function () {
        Route::any('index', [ConfigController::class, 'index']);
        Route::any('edit', [ConfigController::class, 'edit']);
        Route::post('drop', [ConfigController::class, 'drop']);
        Route::post('batchDrop', [ConfigController::class, 'batchDrop']);
    });

    // 配置数据
    Route::prefix('configdata')->group(function () {
        Route::any('index', [ConfigDataController::class, 'index']);
        Route::any('edit', [ConfigDataController::class, 'edit']);
        Route::post('drop', [ConfigDataController::class, 'drop']);
        Route::post('batchDrop', [ConfigDataController::class, 'batchDrop']);
    });

    // 网站配置
    Route::prefix('configweb')->group(function () {
        Route::any('index', [ConfigWebController::class, 'index']);
    });

    // 站点管理
    Route::prefix('item')->group(function () {
        Route::any('index', [ItemController::class, 'index']);
        Route::any('edit', [ItemController::class, 'edit']);
        Route::post('drop', [ItemController::class, 'drop']);
        Route::post('batchDrop', [ItemController::class, 'batchDrop']);
    });

    // 栏目管理
    Route::prefix('itemcate')->group(function () {
        Route::any('index', [ItemCateController::class, 'index']);
        Route::any('edit', [ItemCateController::class, 'edit']);
        Route::post('drop', [ItemCateController::class, 'drop']);
        Route::post('batchDrop', [ItemCateController::class, 'batchDrop']);
        Route::post('getCateList', [ItemCateController::class, 'getCateList']);
    });

    // 广告位管理
    Route::prefix('adsort')->group(function () {
        Route::any('index', [AdSortController::class, 'index']);
        Route::any('edit', [AdSortController::class, 'edit']);
        Route::post('drop', [AdSortController::class, 'drop']);
        Route::post('batchDrop', [AdSortController::class, 'batchDrop']);
    });

    // 广告管理
    Route::prefix('ad')->group(function () {
        Route::any('index', [AdController::class, 'index']);
        Route::any('edit', [AdController::class, 'edit']);
        Route::post('drop', [AdController::class, 'drop']);
        Route::post('batchDrop', [AdController::class, 'batchDrop']);
    });

    // 布局描述
    Route::prefix('layoutdesc')->group(function () {
        Route::any('index', [LayoutDescController::class, 'index']);
        Route::any('edit', [LayoutDescController::class, 'edit']);
        Route::post('drop', [LayoutDescController::class, 'drop']);
        Route::post('batchDrop', [LayoutDescController::class, 'batchDrop']);
        Route::post('getLayoutDescList', [LayoutDescController::class, 'getLayoutDescList']);
    });

    // 布局管理
    Route::prefix('layout')->group(function () {
        Route::any('index', [LayoutController::class, 'index']);
        Route::any('edit', [LayoutController::class, 'edit']);
        Route::post('drop', [LayoutController::class, 'drop']);
        Route::post('batchDrop', [LayoutController::class, 'batchDrop']);
    });

    // 通知公告
    Route::prefix('notice')->group(function () {
        Route::any('index', [NoticeController::class, 'index']);
        Route::any('edit', [NoticeController::class, 'edit']);
        Route::post('drop', [NoticeController::class, 'drop']);
        Route::post('batchDrop', [NoticeController::class, 'batchDrop']);
    });

    // 城市管理
    Route::prefix('city')->group(function () {
        Route::any('index', [CityController::class, 'index']);
        Route::any('edit', [CityController::class, 'edit']);
        Route::post('drop', [CityController::class, 'drop']);
        Route::post('batchDrop', [CityController::class, 'batchDrop']);
        Route::post('getChilds', [CityController::class, 'getChilds']);
    });

    // CMS管理
    Route::prefix('article')->group(function () {
        Route::any('index', [ArticleController::class, 'index']);
        Route::any('edit', [ArticleController::class, 'edit']);
        Route::post('drop', [ArticleController::class, 'drop']);
        Route::post('batchDrop', [ArticleController::class, 'batchDrop']);
    });

    // 统计报表
    Route::prefix('statistics')->group(function () {
        Route::get('index', [StatisticsController::class, 'index']);
    });

    // 代码生成器
    Route::prefix('generate')->group(function () {
        Route::any('index', [GenerateController::class, 'index']);
        Route::post('generate', [GenerateController::class, 'generate']);
        Route::post('batchGenerate', [GenerateController::class, 'batchGenerate']);
    });

});
